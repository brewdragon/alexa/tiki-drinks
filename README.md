# Tiki Drinks
Alexa Tiki Drinks skill.


## Intents
Requests you make to Alexa are called "intents."

Example intents when the skill is not currently open:

> *"Alexa, open Tiki Drinks"*

> *"Alexa, ask Tiki Drinks: How do I make a Mai Tai?"*

> *"Alexa, ask Tiki Drinks for list of recipes"*

> *"Alexa, ask Tiki Drinks for a list of drinks with lime juice"*

Example interactive intents once the skill is open (i.e. after saying
"Alexa, open Tiki Drinks"):

> *"Help"*

> *"Show me a list of drinks"*

> *"How do I make a Mai Tai?"*

> *"Show me the recipe for a Mai Tai?"*

> *"Give me a list of drinks that use lemon juice"*

> *"Exit"*

> *"Cancel"*

## Cards
The text of each fact will printed to a "card" in the Alexa app on your mobile
device. To view the card:

1. Open the Alexa app on your mobile device.
2. Tap the "More" button at the bottom right of the screen.
3. Select "Activity History"

The card should now be visible on your screen.

## Addons
Tiki Drinks has one addon available for purchase called "Recipe Pack."
Purchasing the Recipe Pack gives you unlimited, eternal access to a bunch of
additional recipes plus all future recipes.

From within the skill, you can make the following requests related to the
Recipe Pack addon:

> *"Show me a list of products"*

> *"What addons do I own?"*

> *"Tell me about the Recipe Pack"*

## Roadmap
In no particular order:
* More recipes
* Favorites
* Ratings
* Random recipe
* Upgraded drink images
* Multi-search (>1 item at a time)

## Support
For bugs, feature requests, contributing recipes, etc. you can
[open an issue](https://gitlab.com/brewdragon/alexa/tiki-drinks/-/issues)

## Privacy Policy
Link to PP.
